kext debug
==========
gdb /path/to/my.kext/Contents/MacOS/my

otool -vt /path/to/my.kext/Contents/MacOS/my
    Disassemble the entire kext from 0x0.

nm /path/to/my.kext/Contents/MacOS/my
nm /path/to/my.kext/Contents/MacOS/my | c++filt
    Symbol table for the kext.

panic debug
===========
sudo gdb -c /path/to/core
(gdb) add-symbol-file /Volumes/KernelDebugKit/mach_kernel
(gdb) bt
(gdb) source /Volumes/KernelDebugKit/kgmacros
(gdb) paniclog

find the base address of the kext, if that is the debug target
/Volumes/KernelDebugKit/createsymbolfiles -a x86_64 -s /tmp /path/to/my.kext

(gdb) add-symbol-file /tmp/com.your.kext
